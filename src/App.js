import { Provider } from "react-redux";
import "./App.css";
import Application from "./Components/Application";
import Features from "./Components/Features";
import Header from "./Components/Header";
import MarketUpdate from "./Components/MarketUpdate";
import store from './Redux/store';

function App() {
  return (
    <Provider store = {store}>
      <div className="App min-h-screen">
        <Header />
        <MarketUpdate/>
        <Features/>
        <Application/>
      </div>
    </Provider>
  );
}

export default App;
