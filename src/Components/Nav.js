import React from "react";
import logo from "../Assets/Images/logo.png";

const Nav = () => {
  return (
    <div>
      {/* Mobile Navbar */}
      <div className="px-4 flex justify-between items-center pt-10 md:hidden">
        <span className="flex items-centerr">
          <img src={logo} />
          <h1 className="text-xl font-bold text-white">Cryptous</h1>
        </span>
        <span>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="#FFFFFF"
            viewBox="0 0 24 24"
            stroke-width="1.5"
            stroke="#FFFFFF"
            class="w-6 h-6"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
            />
          </svg>
        </span>
      </div>

      {/* Desktop Navbar */}

      <div className="px-4 hidden md:block container mx-auto max-w-screen-xl  pt-8">
        <div className=" flex items-center justify-between">
          <span className="flex items-centerr">
            <img src={logo} />
            <h1 className="text-xl font-bold text-white">Cryptous</h1>
          </span>

          <ul className="flex text-base text-white gap-x-3">
            <li className="hover:text-[#EB5757] transition-all duration-150">
              <a href="#">Home</a>
            </li>
            <li className="hover:text-[#EB5757] transition-all duration-150">
              <a href="#">Company</a>
            </li>
            <li className="hover:text-[#EB5757] transition-all duration-150">
              <a href="#">About</a>
            </li>
            <li className="hover:text-[#EB5757] transition-all duration-150">
              <a href="#">Pricing</a>
            </li>
            <li className="hover:text-[#EB5757] transition-all duration-150">
              <a href="#">Team</a>
            </li>
            <li className="hover:text-[#EB5757] transition-all duration-150">
              <a href="#">Contact</a>
            </li>
            <li className="hover:text-[#EB5757] transition-all duration-150">
              <a href="#">Blog</a>
            </li>
          </ul>

          <button className="bg-transparent ring-3 hover:bg-[#EB5757] transition-all duration-150    rounded-full text-white px-7 py-2 border-2 border-[#EB5757]">
            Trade Now
          </button>
        </div>
      </div>
    </div>
  );
};

export default Nav;
