const Separator = (number) => {
  var num = number
  var commas = num.toLocaleString("en-US");
  var commas = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return commas
};


const intSeparator = (numb) =>{
  var str = numb.toString().split(".");
  str[0] = str[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return str.join(".");
}

export {Separator,intSeparator}